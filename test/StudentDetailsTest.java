import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import org.fluentlenium.core.FluentPage;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import play.libs.F.Callback;
import play.libs.Yaml;
import play.test.TestBrowser;

import java.util.List;
import java.util.Map;

import static org.fest.assertions.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.withClass;
import static org.fluentlenium.core.filter.FilterConstructor.withId;
import static play.test.Helpers.*;


public class StudentDetailsTest {
    private final int testPort = 3333;

    public void setUp() {
        Map<String, List<Object>> all = (Map<String, List<Object>>) Yaml.load("initial-data.yml");
        SqlUpdate down = Ebean.createSqlUpdate("DELETE FROM ituet_user; DELETE FROM student; DELETE FROM faculty; ");
        down.execute();
        Ebean.save(all.get("faculties"));
        Ebean.save(all.get("students"));
        Ebean.save(all.get("users"));
    }

    /**
     * Test simple retrieval of the index page.
     */
    //@Test
    public void testIndexPageRetrieval() {
        running(testServer(testPort, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
            @Override
            public void invoke(TestBrowser browser) {
                setUp();
                Page page = new Page(browser.getDriver(), testPort, "edit/1");
                browser.goTo(page);
                page.isAt();
            }
        });
    }

    /**
     * Test submission of an empty form.
     */
    //@Test
    public void testIndexPageEmptySubmission() {
        running(testServer(testPort, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
            @Override
            public void invoke(TestBrowser browser) {
                setUp();
                Page indexPage = new Page(browser.getDriver(), testPort, "new");
                browser.goTo(indexPage);
                indexPage.isAt();
                indexPage.submit();
                assertThat(indexPage.hasErrorMessage()).isTrue();
            }
        });
    }

    /**
     * Test submission of a valid form.
     */
   //@Test
    public void testIndexPageValidSubmission() {
        running(testServer(testPort, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
            @Override
            public void invoke(TestBrowser browser) {
                setUp();
                Page indexPage = new Page(browser.getDriver(), testPort, "edit/1");
                browser.goTo(indexPage);
                indexPage.isAt();
                indexPage.submit();
                assertThat(indexPage.hasSuccessMessage()).isTrue();
            }
        });
    }

    /**
     * Test submission of a manually filled out form.
     */
    //@Test
    public void testIndexPageFormFilledSubmission() {
        running(testServer(testPort, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
            @Override
            public void invoke(TestBrowser browser) {
                setUp();
                Page indexPage = new Page(browser.getDriver(), testPort, "edit/1");
                browser.goTo(indexPage);
                indexPage.isAt();
                indexPage.setName("Ronald D. Moore");
                indexPage.setPhone("12345");
                indexPage.submit();
                System.out.println(browser.pageSource());  // useful for debugging.
                assertThat(indexPage.hasSuccessMessage()).isTrue();
            }
        });
    }

    public static class Page extends FluentPage {
        private String url;

        public Page(WebDriver webDriver, int port, String studentID) {
            super(webDriver);
            this.url = "http://localhost:" + port + "/students/" + studentID;
        }

        @Override
        public String getUrl() {
            return this.url;
        }

        @Override
        public void isAt() {
            assertThat(title()).isEqualTo("Hồ sơ học viên");
        }

        public void setName(String name) {
            fill("#name").with(name);
        }

        public void setEmail(String email) {
            fill("#email").with(email);
        }

        public void setPhone(String phone) {
            fill("#phone").with(phone);
        }

        public void submit() {
            submit("#submit");
        }

        public void cancel() {
            find("#cancel").click();
        }

        public boolean hasSuccessMessage() {
            return findFirst("div", withClass("alert alert-success")) != null;
        }

        public boolean hasErrorMessage() {
            return findFirst("div", withClass("alert alert-error")) != null;
        }
    }
}
