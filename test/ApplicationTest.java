import org.junit.Test;

import static org.fest.assertions.Assertions.*;


/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest {

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertThat(a).isEqualTo(2);
    }

    @Test
    public void renderTemplate() {
//        Content html = views.html.index.render("Your new application is ready.");
//        assertThat(contentType(html)).isEqualTo("text/html");
//        assertThat(contentAsString(html)).contains("Your new application is ready.");
    }
//    @Test
//    public void createNewNote() {
//        //Result result;
//        // Should return bad request if no data is given
//        Result result = callAction(
//                controllers.routes.ref.Application.index(),
//                new FakeRequest(GET, "/")
//        );
//        assertThat(status(result)).isEqualTo(OK);

//        result = callAction(
//                controllers.routes.ref.Students.new,
//                fakeRequest().withFormUrlEncodedBody(
//                        ImmutableMap.of("title", "", "text",
//                                "")));
//        assertThat(status(result)).isEqualTo(BAD_REQUEST);
//
//        result = callAction(
//                controllers.routes.ref.Notes.newNote(),
//                fakeRequest().withFormUrlEncodedBody(
//                        ImmutableMap.of("title", "My note title", "text",
//                                "My note content")));
//
//        // Should return redirect status if successful
//        assertThat(status(result)).isEqualTo(SEE_OTHER);
//        assertThat(redirectLocation(result)).isEqualTo("/notes");
//
//        Note newNote = Note.find.where().eq("title", "My note title")
//                .findUnique();
//
//        // Should be saved to DB
//        assertNotNull(newNote);
//        assertEquals("My note title", newNote.title);
//        assertEquals("My note content", newNote.text);
 //   }

}
