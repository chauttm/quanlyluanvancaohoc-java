name := """ituet"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"


libraryDependencies ++= Seq (
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  filters,
  "net.sourceforge.jexcelapi" % "jxl" % "2.6.12",
  "ch.qos.logback" % "logback-classic" % "1.1.1"
  )

libraryDependencies += "ws.securesocial" % "securesocial_2.11" % "3.0-M1"

resolvers += Resolver.sonatypeRepo("snapshots")
