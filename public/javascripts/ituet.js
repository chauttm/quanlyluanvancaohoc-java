function del ( urlToDelete ) {
        $.ajax ( {
            url : urlToDelete,
            type : 'DELETE',
            success : function ( results ) {
                // Refresh the page
                location.reload ( ) ;
            }
        } ) ;
    }

    jQuery(document).ready(function ($) {
        $(function () {
            //$(".needsDatepicker").datepicker({ dateFormat: 'dd/mm/yy' });
            $(".needsDatepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        });
    });