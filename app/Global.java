import com.avaje.ebean.Ebean;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import models.Faculty;
import models.ItuetUser;
import models.Student;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.api.mvc.EssentialFilter;
import play.filters.csrf.CSRFFilter;
import play.libs.Yaml;
import securesocial.core.RuntimeEnvironment;
import service.MyEnvironment;


//import securesocial.core.RuntimeEnvironment;
//import service.MyEnvironment;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Global extends GlobalSettings {

    public void onStart(Application app) {
        Logger.info("Application has started");
        InitialData.insert(app);
    }

    static class InitialData {
        public static void insert(Application app) {
            Map<String, List<Object>> all =
                    (Map<String, List <Object>>) Yaml.load("initial-data.yml");
            if (Ebean.find(Faculty.class).findRowCount() < 1) {
                Ebean.save(all.get("faculties"));
            }
            if (Ebean.find(Student.class).findRowCount() < 1) {
                Ebean.save(all.get("students"));
            }
            if (Ebean.find(ItuetUser.class).findRowCount() < 1) {
                Ebean.save(all.get("users"));
            }

//            if (Ebean.find(Student.class).findRowCount() == 0) {
//                Ebean.save(all.get("students"));
//            }
//
//            if (Ebean.find(Student.class).findRowCount() == 0) {
//                try {
//                    read("K19.xls");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }

    static private void read(String inputFile) throws IOException {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        try {
            w = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet(0);
            for (int i = 3; i < 80; i++) {
                readStudentData(sheet, i);
            }

            sheet = w.getSheet(1);
            for (int i = 5; i < 15; i++) {
                readStudentData(sheet, i);
            }

            sheet = w.getSheet(2);
            for (int i = 5; i < 95; i++) {
                readStudentData(sheet, i);
            }

            sheet = w.getSheet(3);
            for (int i = 5; i < 35; i++) {
                readStudentData(sheet, i);
            }

        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    private static void readStudentData(Sheet sheet, int row) {
        Student student = new Student();
        student.number = readCell(sheet,1, row);
        student.name = readCell(sheet,2, row);
        String s = sheet.getCell(3, row).getContents();
        student.birthDate = s.isEmpty() ? null : new Date(s);
        student.gender = readCell(sheet,4, row);
        student.birthPlace = readCell(sheet,5, row);
        student.ethnic = readCell(sheet,6, row);
        student.religion = readCell(sheet,7, row);
        student.dtut = readCell(sheet,8, row);
        student.dtdt = readCell(sheet,9, row);
        student.doan = readCell(sheet,10, row);
        student.dang = readCell(sheet,11, row);
        student.major = readCell(sheet,12, row);
        student.dot = readCell(sheet,13, row);
        student.notes = readCell(sheet,14, row);
        student.thesisTitle = readCell(sheet,15, row);
        student.phone = readCell(sheet,18, row);
        student.email = readCell(sheet,19, row);
        student.batch = 19;

        String faculty_name = readCell(sheet,16, row);
        if (faculty_name != null) {
            Faculty f = (Faculty) Faculty.findFirstByName(faculty_name);
            if (f == null) {
                f = new Faculty();
                f.name = faculty_name;
                f.workPlace = readCell(sheet,17, row);
                Ebean.save(f);
            }
            student.advisor = f;
        }
        System.out.println(student);
        Ebean.save(student);
    }

    private static String readCell(Sheet sheet,  int col, int row) {
        String input = sheet.getCell(col, row).getContents();
        return (input.isEmpty() ? null : input);
    }

    public static void main(String[] args) {
        try {
            read("D:\\chauttm\\play\\ituet\\conf\\K19.xls");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{CSRFFilter.class};
        //return new Class[]{};
    }

    private RuntimeEnvironment env = new MyEnvironment();

    @Override
    public <A> A getControllerInstance(Class<A> controllerClass) throws Exception {
        A result;

        try {
            result = controllerClass.getDeclaredConstructor(RuntimeEnvironment.class).newInstance(env);
        } catch (NoSuchMethodException e) {
            // the controller does not receive a RuntimeEnvironment, delegate creation to base class.
            result = super.getControllerInstance(controllerClass);
        }
        return result;
    }
}