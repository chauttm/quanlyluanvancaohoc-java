package controllers;

import com.avaje.ebean.Page;
import models.Faculty;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import securesocial.core.java.SecuredAction;
import views.html.faculties.details;
import views.html.faculties.list;

/**
 * Created by dse on 3/30/2015.
 */
@With(CurrentUser.class)
public class Faculties  extends Controller {
    private static final Form<Faculty> facultyForm = Form.form(Faculty.class);

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, responses = Application.class)
    public static Result list(Integer page) {
        Page<Faculty> faculties = Faculty.find(page);
        return ok(list.render(faculties));
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result newFaculty() {
        return ok(details.render(null, facultyForm, false));
    }

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, responses = Application.class)
    public static Result details(Faculty faculty) {
        return renderDetailPage(faculty, true);
    }

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, responses = Application.class)
    public static Result edit(Faculty faculty) {
        return renderDetailPage(faculty, false);
    }

    private static Result renderDetailPage(Faculty faculty, boolean readonly) {
        if (faculty == null) {
            return notFound(String.format("Không tìm thấy hồ sơ cán bộ."));
        }
        Form<Faculty> filledForm = facultyForm.fill(faculty);
        return ok(details.render(faculty, filledForm, readonly));
    }

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, responses = Application.class)
    public static Result save() {
        Form<Faculty> boundForm = facultyForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(null, boundForm, false));
        }
        Faculty faculty = boundForm.get();


        if (faculty.id == null) {
            faculty.save();
        } else {
            faculty.update();
        }

        flash("success", String.format("Đã tạo thành công hồ sơ cán bộ %s", faculty));
        return redirect(routes.Faculties.list(0));
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result delete(Long id) {
        Faculty faculty = Faculty.findById(id);
        if (faculty == null) {
            return notFound(String.format("Không tìm thấy hồ sơ cán bộ.", id));
        }

        faculty.delete();
        return redirect(routes.Faculties.list(0));
    }
}