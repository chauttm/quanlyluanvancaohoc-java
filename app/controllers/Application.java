package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Image;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;

import play.twirl.api.Html;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredActionResponses;
import securesocial.core.java.UserAwareAction;
import views.html.*;

import static play.data.Form.form;

@With(CurrentUser.class)
public class Application extends Controller implements SecuredActionResponses {

    //@UserAwareAction
    public static Result index() {
        return ok(index.render(form(UploadImageForm.class),Image.find.all()
        ));
    }

    public static Result getImage(long id) {
        Image image = Image.find.byId(id);

        if (image != null) {

            /*** here happens the magic ***/
            return ok(image.data).as("image");
            /************************** ***/

        } else {
            flash("error", "Picture not found.");
            return redirect(routes.Application.index());
        }
    }

    public static Result uploadImage() {
        Form<UploadImageForm> form = form(UploadImageForm.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(index.render(form, Image.find.all()));

        } else {
            new Image(
                    form.get().image.getFilename(),
                    form.get().image.getFile()
            );

            flash("success", "File uploaded.");
            return redirect(routes.Application.index());
        }
    }

    public static class UploadImageForm {
        public Http.MultipartFormData.FilePart image;

        public String validate() {
            Http.MultipartFormData data = request().body().asMultipartFormData();
            image = data.getFile("image");

            if (image == null) {
                return "File is missing.";
            }

            return null;
        }
    }

    // SecuredActionResponses methods
    public Html notAuthorizedPage(Http.Context ctx) {
        return views.html.custom.notAuthorized.render(ctx._requestHeader(), SecureSocial.env());
    }

    public F.Promise<Result> notAuthenticatedResult(Http.Context ctx) {
        Http.Request req = ctx.request();
        Result result;

        if ( req.accepts("text/html")) {
            ctx.flash().put("error", play.i18n.Messages.get("securesocial.loginRequired"));
            ctx.session().put(SecureSocial.ORIGINAL_URL, ctx.request().uri());
            result = redirect(SecureSocial.env().routes().loginPageUrl(ctx._requestHeader()));
        } else if ( req.accepts("application/json")) {
            ObjectNode node = Json.newObject();
            node.put("error", "Credentials required");
            result = unauthorized(node);
        } else {
            result = unauthorized("Credentials required");
        }
        return F.Promise.pure(result);
    }

    public F.Promise<Result> notAuthorizedResult(Http.Context ctx) {
        Http.Request req = ctx.request();
        Result result;

        if ( req.accepts("text/html")) {
            result = forbidden(notAuthorizedPage(ctx));
        } else if ( req.accepts("application/json")) {
            ObjectNode node = Json.newObject();
            node.put("error", "Not authorized");
            result = forbidden(node);
        } else {
            result = forbidden("Not authorized");
        }

        return F.Promise.pure(result);
    }
}
