package controllers;

import models.Seminar;
import models.Student;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import securesocial.core.java.SecuredAction;
import utils.Mailer;
import views.html.seminars.confirmationFeedback;
import views.html.seminars.confirmationRequest;
import views.html.seminars.details;
import views.html.seminars.list;

import java.util.Date;
import java.util.List;

/**
 * Created by dse on 3/30/2015.
 */
@With(CurrentUser.class)
public class Seminars  extends Controller {
    private static final Form<Seminar> seminarForm = Form.form(Seminar.class);

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, responses = Application.class)
    public static Result list() {
        List<Seminar> seminars = Seminar.findAll();
        return ok(list.render(seminars, Student.find.all().get(0), true));
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result requestConfirmations(long id) {
        Seminar seminar = Seminar.findById(id);
        if (seminar == null) {
            flash("error", "Không tìm thấy xê-mi-na.");
            return list();
        }
        StringBuilder error = new StringBuilder();
        int emailCount = 0;
        for (Student student: seminar.students) {
            if (student.needsSeminarConfirmation()) {
                if (student.advisor != null && student.advisor.email != null) {
                    if (requestAdvisorConfirmation(student, seminar.date)) {
                        emailCount++;
                    }
                } else {
                    error.append(" ").append(student.name);
                }
            }
        }
        if (error.length()>0) {
            flash("error", "Không tìm thấy email giáo viên hướng dẫn của (các) học viên sau:" + error.toString());
        }
        flash("success", "Đã gửi " + emailCount + " email đề nghị duyệt đăng ký");

        return redirect(routes.Seminars.details(seminar));
    }

    public static Result confirm(String code) {
        Student student = Student.findBySeminarConfirmationCode(code);
        if (student == null) {
            return notFound(String.format("Không tìm thấy đăng kí của học viên"));
        }
        student.confirmSeminarRegistration();
        return ok(confirmationFeedback.render(student));
    }

    private static boolean requestAdvisorConfirmation(Student student, Date date) {
        try {
            Mailer.send(student.advisor.email, "Xemina", confirmationRequest.render(student).toString());
        } catch (RuntimeException e) {
            flash("error", "Lỗi gửi email tới " + student.advisor.email);
            return false;
        }
        return true;
    }

    @SecuredAction(authorization = Authorizations.AdminOrStudent.class, responses = Application.class)
    public static Result register(Long id, Long studentID) {
        Seminar seminar = Seminar.findById(id);
        if (seminar == null) {
            return notFound(String.format("Không tìm thấy xê-mi-na."));
        }
        Student student = Student.findById(studentID);
        if (student == null) {
            return notFound(String.format("Không tìm thấy hồ sơ học viên."));
        }
        student.registerSeminar(seminar);
        return list();
    }

    @SecuredAction(authorization = Authorizations.AdminOrStudent.class, responses = Application.class)
    public static Result unregister(Long studentID) {
        Student student = Student.findById(studentID);
        if (student == null) {
            return notFound(String.format("Không tìm thấy hồ sơ học viên."));
        }
        student.unregisterSeminar();
        return list();
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result newSeminar() {
        return ok(details.render(null, seminarForm, true, true));
    }

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, responses = Application.class)
    public static Result details(Seminar seminar) {
        return renderDetailPage(seminar, true, false);
    }

    @SecuredAction(authorization = Authorizations.AdminOrStudent.class, responses = Application.class)
    public static Result edit(Seminar seminar) {
        return renderDetailPage(seminar, true, true);
    }

    private static Result renderDetailPage(Seminar seminar, boolean adminMode, boolean editMode) {
        if (seminar == null) {
            return notFound(String.format("Không tìm thấy xê-mi-na."));
        }
        Form<Seminar> filledForm = seminarForm.fill(seminar);
        return ok(details.render(seminar, filledForm, adminMode, editMode));
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result save() {
        Form<Seminar> boundForm = seminarForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(null,boundForm, true, true));
        }
        Seminar seminar = boundForm.get();

        if (seminar.id == null) {
            seminar.save();
        } else {
            seminar.update();
        }

        flash("success", String.format("Đã tạo được xê-mi-na %s", seminar));
        return redirect(routes.Seminars.list());
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result delete(Long id) {
        Seminar seminar = Seminar.findById(id);
        if (seminar == null) {
            return notFound(String.format("Không tìm thấy xê-mi-na."));
        }

        seminar.delete();
        return redirect(routes.Seminars.list());
    }
}