package controllers;

import com.avaje.ebean.Page;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Student;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import models.ItuetUser;
import securesocial.core.java.SecuredActionResponses;
import securesocial.core.java.UserAwareAction;
import service.ItuetUserService;
import views.html.students.*;

/**
 * Created by dse on 3/27/2015.
 */
@With(CurrentUser.class)
public class Students extends Controller  {
    private static final Form<Student> studentForm = Form.form(Student.class);

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, params = {}, responses = Application.class)
    public static Result list(Integer page) {
        Page<Student> students = Student.find(page);
        return ok(list.render(students));
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result newStudent() {
        return ok(details.render(null, studentForm, true));
    }

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, params = {}, responses = Application.class)
    public static Result details(Student student) {
        if (student == null) {
            return notFound(String.format("Không tìm thấy hồ sơ học viên."));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(student, filledForm, false));
    }

    @SecuredAction(authorization = Authorizations.RegisteredUser.class, params = {}, responses = Application.class)
    public static Result edit(Student student) {
        if (student == null) {
            return notFound("Không tìm thấy hồ sơ học viên.");
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(student, filledForm, true));
    }

    @SecuredAction(authorization = Authorizations.AdminOrStudent.class, responses = Application.class)
    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(null, boundForm, false));
        }
        Student student = boundForm.get();

        ItuetUser user = (ItuetUser)ctx().args.get("current_user");
        if (user.student!=null && !user.student.id.equals(student.id)) {
            flash("error", "Bạn không có quyền sửa thông tin của học viên khác.");
            return redirect(routes.Application.index());
        }

        if (student.id == null) {
            student.save();
        } else {
            student.update();
        }

        flash("success", String.format("Sửa thành công thông tin học viên %s", student.name));
        return redirect(routes.Students.list(0));
    }

    @SecuredAction(authorization = Authorizations.Admin.class, responses = Application.class)
    public static Result delete(String ean) {
        Student student = Student.findByNumber(ean);
        if (student == null) {
            return notFound("Không tìm thấy hồ sơ học viên.");
        }

        student.delete();
        return redirect(routes.Students.list(0));
    }
}
