package controllers;

import models.ItuetUser;
import play.Logger;
import securesocial.core.java.Authorization;

public class Authorizations {
    public static class RegisteredUser implements Authorization<ItuetUser> {
        public boolean isAuthorized(ItuetUser user, String params[]) {
            return user != null;
        }
    }

    public static class Admin implements Authorization<ItuetUser> {
        @Override
        public boolean isAuthorized(ItuetUser ituetUser, String[] strings) {
            Logger.debug(ituetUser.email);
            return ituetUser.isAdmin;
        }
    }

    public static class AdminOrStudent implements Authorization<ItuetUser> {
        @Override
        public boolean isAuthorized(ItuetUser ituetUser, String[] strings) {
            Logger.debug(ituetUser.email);
            return ituetUser.isAdmin || ituetUser.isStudent();
        }
    }

    public static class Student implements Authorization<ItuetUser> {
        @Override
        public boolean isAuthorized(ItuetUser ituetUser, String[] strings) {
            return ituetUser.isStudent();
        }
    }

    public static class Faculty implements Authorization<ItuetUser> {
        @Override
        public boolean isAuthorized(ItuetUser ituetUser, String[] strings) {
            return ituetUser.isFaculty();
        }
    }
}