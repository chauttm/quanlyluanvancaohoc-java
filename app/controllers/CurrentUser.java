package controllers;

import models.ItuetUser;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;

/**
 * Created by dse on 4/26/2015.
 */
public class CurrentUser extends Action.Simple {

    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        ItuetUser user = (ItuetUser)ctx.args.get(SecureSocial.USER_KEY);
        if (user != null) {
            ctx.args.put("current_user", user);
        }
        return delegate.call(ctx);
    }

    public static ItuetUser get() {
        return (ItuetUser)Http.Context.current().args.get("current_user");
    }
}