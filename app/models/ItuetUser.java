package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import securesocial.core.BasicProfile;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ItuetUser extends Model {
    public ItuetUser(BasicProfile user) {
        this.main = new UserProfile(user);
        identities = new ArrayList<UserProfile>();
        identities.add(main);
    }

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;
    @Constraints.Required
    public String email;

    public boolean isAdmin = false;

    @OneToOne
    public Student student;

    @OneToOne
    public Faculty faculty;

    @OneToOne
    public UserProfile main;

    @OneToMany (mappedBy = "ituetUser")
    public List<UserProfile> identities;

    public static Finder<Long, ItuetUser> find = new Finder<Long, ItuetUser>(
            Long.class, ItuetUser.class
    );

    public ItuetUser(String email, Boolean isAdmin, Student student, Faculty faculty) {
        this.email = email;
        this.isAdmin = isAdmin != null && isAdmin.booleanValue();
        this.student = student;
        this.faculty = faculty;
    }

    public static ItuetUser findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    public boolean isStudent() {
        return student != null;
    }

    public boolean isFaculty() {
        return faculty != null;
    }

    public void addProfile(BasicProfile to) {
        UserProfile userProfile = new UserProfile(to);
        userProfile.ituetUser = this;
        userProfile.save();

        if (main == null) {
            main = userProfile;
        }
        identities.add(new UserProfile(to));

        update();
    }
}