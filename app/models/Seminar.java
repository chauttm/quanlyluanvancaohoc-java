package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.*;
import java.util.*;

@Entity
public class Seminar extends Model implements PathBindable<Seminar> {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

	@Constraints.Required
    @Column(nullable=false)
    //@Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;
    
    @Constraints.Required
    @Column(nullable=false)
    public String description;

    @OneToMany(mappedBy = "latestSeminar")
    public List<Student> students;
    
    public Date openDate;
    public Date closedDate;
    
    public static Finder<Long, Seminar> find = new Finder<Long, Seminar>(
            Long.class, Seminar.class
    );

    public Seminar() {
 
    }

    public String toString() {
        return String.format("%s ngày %td/%tm/%tY", description, date,date,date);
    }
    
    public int studentCount() {
        return students.size();
    }

    public static List<Seminar> findAll() {
        return find.where().orderBy("date desc").findList();
    }

    public static Seminar findById(Long id) {
        return find.where().eq("id", id).findUnique();
    }

    @Override
    public Seminar bind(String key, String value) {
        return findById(Long.parseLong(value));
    }

    @Override
    public String unbind(String key) {
        return id+"";
    }

    @Override
    public String javascriptUnbind() {
        return id+"";
    }
}