package models;

import play.db.ebean.Model;
import play.libs.Scala;
import securesocial.core.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by dse on 4/8/2015.
 */
@Entity
public class UserProfile extends Model implements Serializable {
    @Id
    public Long id;

    @ManyToOne
    public ItuetUser ituetUser;

    public String providerId;
    public String userId;
    public String firstName;
    public String lastName;
    public String fullName;
    public String avatarUrl;
    public String email;
    public String authMethod;
//    public OAuth1Info oAuth1Info;
//    public OAuth2Info oAuth2Info;
//    public PasswordInfo passwordInfo;

    public static Model.Finder<Long, UserProfile> find = new Model.Finder<Long, UserProfile>(
            Long.class, UserProfile.class
    );

    public static UserProfile findByProviderIdAndUserId(String providerId, String userId) {
        return find.where()
                .eq("providerId", providerId)
                .eq("userId", userId)
                .findUnique();
    }

    public UserProfile(BasicProfile profile) {
        providerId = profile.providerId();
        userId = profile.userId();
        firstName = profile.firstName().get();
        lastName = profile.lastName().get();
        fullName = profile.fullName().get();
        email = profile.email().get();
        avatarUrl = profile.avatarUrl().isDefined() ? profile.avatarUrl().get() : null;
        authMethod = profile.authMethod().method();
//        oAuth1Info = profile.oAuth1Info().get();
//        oAuth2Info = profile.oAuth2Info().get();
//        passwordInfo = profile.passwordInfo().get();
    }

    public BasicProfile toBasicProfile() {
        return BasicProfile$.MODULE$.apply(
                providerId,
                userId,
                Scala.Option(firstName),
                Scala.Option(lastName),
                Scala.Option(fullName),
                Scala.Option(email),
                Scala.Option(avatarUrl),
                AuthenticationMethod.UserPassword(),
                Scala.<OAuth1Info>None(),
                Scala.<OAuth2Info>None(),
                Scala.<PasswordInfo>None());
    }

    public void updateWith(BasicProfile profile) {
        firstName = profile.firstName().get();
        lastName = profile.lastName().get();
        fullName = profile.fullName().get();
        email = profile.email().get();
        avatarUrl = profile.avatarUrl().isDefined() ? profile.avatarUrl().get() : null;
        authMethod = profile.authMethod().method();
        update();
    }
}
