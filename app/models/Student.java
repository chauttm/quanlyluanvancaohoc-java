package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.*;
import java.util.*;

/**
 * Created by dse on 3/27/2015.
 */
@Entity
public class Student extends Model implements PathBindable<Student> {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @Constraints.Required
    @Column(unique=true, nullable=false)
    public String number;

    @Constraints.Required
    @Column(nullable=false)
    public String name;

    @OneToOne(mappedBy = "student")
    public ItuetUser user;

    public String birthPlace;
    //@Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date birthDate;
    public String gender;
    public String thesisTitle;

    @ManyToOne
    public Faculty advisor;

    @ManyToOne
    public Faculty coAdvisor;
    
    @ManyToOne
    public Seminar latestSeminar;
    public String seminarConfirmationCode; //null means approved

    public static final String NEW = "N";
    public static final String PROPOSED = "P";
    public static final String ACCEPTED = "A";
    public static final String SEMINAR = "S";
    public static final String READY = "R";
    public static final String FINISHED = "F";
    public String status = NEW;

    public String phone;
    public String major;
    public String notes;
    public int batch;
    @Constraints.Required
    @Column(unique=true, nullable=false)
    public String email;
    public String ethnic;
    public String religion;
    public String dtut;
    public String dtdt;
    public String doan;
    public String dang;
    public String dot;
    
    public String comments;

    public static Finder<Long, Student> find = new Finder<Long, Student>(
            Long.class, Student.class
    );

    public Student() {
    }

    public Student(String ean, String name, String birthPlace) {
        this.number = ean;
        this.name = name;
        this.birthPlace = birthPlace;
    }

    public static final Map<String, String> statusOptions;
    public static final Map<String, String> genderOptions;
    public static final Map<String, String> majorOptions;
    static
    {
        statusOptions = new HashMap<String, String>();
        statusOptions.put(NEW, "1. chưa nộp đề cương");
        statusOptions.put(PROPOSED, "2. đã nộp đề cương");
        statusOptions.put(ACCEPTED, "3. đề cương đã được duyệt");
        statusOptions.put(SEMINAR, "4. đã báo cáo xêmina");
        statusOptions.put(READY, "5. chuẩn bị bảo vệ");
        statusOptions.put(FINISHED, "6. đã bảo vệ xong");

        genderOptions = new HashMap<String, String>();
        genderOptions.put("Nam", "Nam");
        genderOptions.put("Nữ", "Nữ");

        majorOptions = new HashMap<String, String>();
        majorOptions.put("HTTT", "Hệ thống thông tin");
        majorOptions.put("KHMT", "Khoa học máy tính");
        majorOptions.put("KTPM", "Kĩ thuật phần mềm");
        majorOptions.put("TDL&MMT", "Truyền thông dữ liệu và Mạng máy tính");
    }

    public String toString() {
        return String.format("%s - %s - $s", number, name, birthPlace);
    }

    public static List<Student> findAll() {
        return find.all();
    }

    public static Page<Student> find(int page) {
        return find.where()
                .orderBy("id asc")
                .findPagingList(5)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Student findByNumber(String ean) {
        return find.where().eq("number", ean).findUnique();
    }
	
	public static Student findById(Long id) {
        return find.where().eq("id", id).findUnique();
    }

    public static List<Student> findByName(String term) {
        return find.where().eq("name", term).findList();
    }

    @Override
    public Student bind(String key, String value) {
        return findByNumber(value);
    }

    @Override
    public String unbind(String key) {
        return number;
    }

    @Override
    public String javascriptUnbind() {
        return number;
    }

    public void registerSeminar(Seminar seminar) {
        latestSeminar = seminar;
        seminarConfirmationCode = number + "abc";
        update();
    }

    public void unregisterSeminar() {
        latestSeminar = null;
        seminarConfirmationCode = null;
        update();
    }

    public static Student findBySeminarConfirmationCode(String code) {
        return find.where().eq("seminarConfirmationCode", code).findUnique();
    }

    public void confirmSeminarRegistration() {
        seminarConfirmationCode = null;
        update();
    }

    public boolean needsSeminarConfirmation() {
        return latestSeminar != null && seminarConfirmationCode != null;
    }
}