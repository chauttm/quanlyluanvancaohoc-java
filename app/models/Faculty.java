package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dse on 3/27/2015.
 */
@Entity
public class Faculty extends Model implements PathBindable<Faculty> {
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long id;

    @Constraints.Required
    public String name;

    @Column(unique=true, nullable=false)
    public String email;

    public Boolean isInternal = true;

    @OneToOne(mappedBy = "faculty")
    public ItuetUser user;

    @OneToMany(mappedBy = "advisor")
    public List<Student> advisedMasterStudents;

    @OneToMany(mappedBy = "coAdvisor")
    public List<Student> coAdvisedMasterStudents;

    public String workPlace;

    public String phone;
    public String hh = "";
    public String hv = "";

    public static final Map<String, String> hhOptions;
    public static final Map<String, String> hvOptions;
    static
    {
        hhOptions = new HashMap<>();
        hhOptions.put("GS.", "Giáo sư");
        hhOptions.put("PGS.", "Phó Giáo sư");

        hvOptions = new HashMap<>();
        hvOptions.put("TS.", "Giáo sư");
        hvOptions.put("ThS.", "Thạc sỹ");
        hvOptions.put("CN.", "Cử nhân");
        hvOptions.put("KS.", "Kỹ sư");
    }

    public String namePlus() {
        return hh + hv + " " + name;
    }

    public Integer studentCount() {
        return advisedMasterStudents.size() + coAdvisedMasterStudents.size();
    }

    public static Finder<Long, Faculty> find = new Finder<Long, Faculty>(
            Long.class, Faculty.class
    );
    public static List<Faculty> findAll() {
        return find.all();
    }


    public static Page<Faculty> find(int page) {
        return find.where()
                .orderBy("name asc")
                .findPagingList(100)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Faculty c: Faculty.find.orderBy("name").findList()) {
            options.put(c.id.toString(), c.name);
        }
        return options;
    }

    public static Faculty findById(Long id) {
        return find.where().eq("id", id).findUnique();
    }

    public static List<Faculty> findByName(String term) {
        return find.where().eq("name", term).findList();
    }
    public static Faculty findFirstByName(String term) {
        return find.where().eq("name", term).findUnique();
    }

    @Override
    public Faculty bind(String key, String value) {
        return findById(Long.parseLong(value));
    }

    @Override
    public String unbind(String key) {
        return id+"";
    }

    @Override
    public String javascriptUnbind() {
        return id+"";
    }
}
