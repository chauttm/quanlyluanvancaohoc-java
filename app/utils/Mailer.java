package utils;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class Mailer
{
    private static Session session = null;

    private static void initSession() {
        if (session == null) {
            Properties props = new Properties();
            props.put("mail.smtp.host", "ctmail.vnu.edu.vn");
            props.put("mail.smtp.socketFactory.port", "25");
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "25");

            session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("chauttm", "password");
                        }
                    });
        }
    }

    public static void send(String to, String subject, String content) {
        try {
            initSession();
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from@no-spam.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setContent(content, "text/html; charset=utf-8");

            Transport.send(message);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static void main(String [] args)
    {
        send("chauttm.vnu@gmail.com", "Hi", "<h1>Test</h1>");
    }

}