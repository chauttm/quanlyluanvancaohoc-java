package service


import models.ItuetUser
import securesocial.controllers.ViewTemplates
import securesocial.core.RuntimeEnvironment
import securesocial.core.providers.{GoogleProvider, TwitterProvider,FacebookProvider}
import securesocial.core.services.UserService

import scala.collection.immutable.ListMap

class MyEnvironment extends RuntimeEnvironment.Default[ItuetUser] {
  override val userService: UserService[ItuetUser] = new ItuetUserService()
  override lazy val viewTemplates: ViewTemplates = new AuthenViewTemplates(this)

  override lazy val providers = ListMap(
    include(new GoogleProvider(routes, cacheService,oauth2ClientFor(GoogleProvider.Google))),
    include(new FacebookProvider(routes, cacheService,oauth2ClientFor(FacebookProvider.Facebook))),
    include(new TwitterProvider(routes, cacheService, oauth1ClientFor(TwitterProvider.Twitter))))
}