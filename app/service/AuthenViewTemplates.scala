package service

import play.api.data.Form
import play.api.i18n.Lang
import play.api.mvc.RequestHeader
import play.api.templates._
import securesocial.controllers.{ChangeInfo, RegistrationInfo, ViewTemplates}

/**
 * Created by dse on 4/10/2015.
 */
class AuthenViewTemplates (env: MyEnvironment) extends ViewTemplates{

  implicit val implicitEnv = env

  override def getLoginPage(form: Form[(String, String)],
                            msg: Option[String] = None)(implicit request: RequestHeader, lang: Lang): Html = {

    views.html.custom.login(form, msg, request, env)
  }

  override def getSignUpPage(form: Form[RegistrationInfo], token: String)(implicit request: RequestHeader, lang: Lang): Html = {

    views.html.students.simple_list(null)

  }

  override def getStartSignUpPage(form: Form[String])(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.students.simple_list(null)
  }

  override def getStartResetPasswordPage(form: Form[String])(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.students.simple_list(null)
  }

  override def getResetPasswordPage(form: Form[(String, String)], token: String)(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.students.simple_list(null)
  }

  override def getPasswordChangePage(form: Form[ChangeInfo])(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.students.simple_list(null)
  }

  override def getNotAuthorizedPage(implicit request: RequestHeader, lang: Lang): Html = {
    views.html.custom.notAuthorized()
  }
}
