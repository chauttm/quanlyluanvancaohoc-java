/**
 * Copyright 2012-2014 Jorge Aliss (jaliss at gmail dot com) - twitter: @jaliss
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package service;

import models.ItuetUser;
import models.UserProfile;
import play.Logger;
import play.libs.F;
import play.mvc.Http;
import securesocial.core.BasicProfile;
import securesocial.core.PasswordInfo;
import securesocial.core.java.SecureSocial;
import securesocial.core.services.SaveMode;
import securesocial.core.java.BaseUserService;
import securesocial.core.java.Token;
import securesocial.core.providers.UsernamePasswordProvider;

import java.util.HashMap;
import java.util.Iterator;

/**
 * A Sample In Memory user service in Java
 * <p/>
 * Note: This is NOT suitable for a production environment and is provided only as a guide.
 * A real implementation would persist things in a database
 */
public class ItuetUserService extends BaseUserService<ItuetUser> {
    public Logger.ALogger logger = play.Logger.of("application.service.ItuetUserService");

    @Override
    public F.Promise<ItuetUser> doSave(BasicProfile profile, SaveMode mode) {
        logger.debug("doSave ");
        ItuetUser result = null;
        if (mode == SaveMode.SignUp()) {
            logger.debug("doSave - SaveModeSignUp");
            result = ItuetUser.findByEmail(profile.email().get());
            result.addProfile(profile);
        } else if (mode == SaveMode.LoggedIn()) {
            logger.debug("doLink - SaveModeLoggedIn");

            UserProfile userProfile = UserProfile.findByProviderIdAndUserId(profile.providerId(), profile.userId());
            if (userProfile != null) {
                userProfile.updateWith(profile);
                result = userProfile.ituetUser;
            }
        } else {
            throw new RuntimeException("Unknown mode");
        }
        return F.Promise.pure(result);
    }

    @Override
    public F.Promise<ItuetUser> doLink(ItuetUser current, BasicProfile to) {
        logger.debug("doLink ");

        ItuetUser target = null;

        if (target == null) {
            // this should not happen
            throw new RuntimeException("Can't find user : " + current.main.userId);
        }

        UserProfile userProfile = UserProfile.findByProviderIdAndUserId(to.providerId(), to.userId());
        if (userProfile == null) {
            target.addProfile(to);
        } else {
            if (!userProfile.ituetUser.id.equals(target.id)) {
                // this should not happen
                throw new RuntimeException("Mixed user profile : " + target.id + " ");
            }
        }

        return F.Promise.pure(target);
    }

    @Override
    public F.Promise<Token> doSaveToken(Token token) {
//        tokens.put(token.uuid, token);
//        return F.Promise.pure(token);
        return F.Promise.pure(null);
    }

    @Override
    public F.Promise<BasicProfile> doFind(String providerId, String userId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Finding user " + userId);
        }

        BasicProfile found = null;
        UserProfile userProfile = UserProfile.findByProviderIdAndUserId(providerId, userId);
        if (userProfile != null) {
            found = userProfile.toBasicProfile();
        }
        return F.Promise.pure(found);
    }

    @Override
    public F.Promise<PasswordInfo> doPasswordInfoFor(ItuetUser user) {
        throw new RuntimeException("doPasswordInfoFor is not implemented yet in sample app");
    }

    @Override
    public F.Promise<BasicProfile> doUpdatePasswordInfo(ItuetUser user, PasswordInfo info) {
        throw new RuntimeException("doUpdatePasswordInfo is not implemented yet in sample app");
    }

    @Override
    public F.Promise<Token> doFindToken(String tokenId) {
        //return F.Promise.pure(tokens.get(tokenId));
        return F.Promise.pure(null);
    }


    @Override
    public F.Promise<BasicProfile> doFindByEmailAndProvider(String email, String providerId) {
        BasicProfile found = null;

//        for ( User u: users.values() ) {
//            for ( BasicProfile i : u.identities ) {
//                if ( i.providerId().equals(providerId) && i.email().isDefined() && i.email().get().equals(email) ) {
//                    found = i;
//                    break;
//                }
//            }
//        }

        return F.Promise.pure(found);
    }

    @Override
    public F.Promise<Token> doDeleteToken(String uuid) {
        //return F.Promise.pure(tokens.remove(uuid));
        return F.Promise.pure(null);
    }

    @Override
    public void doDeleteExpiredTokens() {
//        Iterator<Map.Entry<String,Token>> iterator = tokens.entrySet().iterator();
//        while ( iterator.hasNext() ) {
//            Map.Entry<String, Token> entry = iterator.next();
//            if ( entry.getValue().isExpired() ) {
//                iterator.remove();
//            }
//        }
    }
}