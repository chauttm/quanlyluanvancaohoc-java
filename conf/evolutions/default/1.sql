# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table faculty (
  id                        bigint not null,
  name                      varchar(255),
  email                     varchar(255) not null,
  is_internal               boolean,
  work_place                varchar(255),
  phone                     varchar(255),
  hh                        varchar(255),
  hv                        varchar(255),
  constraint uq_faculty_email unique (email),
  constraint pk_faculty primary key (id))
;

create table image (
  id                        bigint not null,
  name                      varchar(255),
  data                      bytea,
  constraint pk_image primary key (id))
;

create table ituet_user (
  id                        bigint not null,
  email                     varchar(255),
  is_admin                  boolean,
  student_id                bigint,
  faculty_id                bigint,
  main_id                   bigint,
  constraint pk_ituet_user primary key (id))
;

create table seminar (
  id                        bigint not null,
  date                      timestamp not null,
  description               varchar(255) not null,
  open_date                 timestamp,
  closed_date               timestamp,
  constraint pk_seminar primary key (id))
;

create table student (
  id                        bigint not null,
  number                    varchar(255) not null,
  name                      varchar(255) not null,
  birth_place               varchar(255),
  birth_date                timestamp,
  gender                    varchar(255),
  thesis_title              varchar(255),
  advisor_id                bigint,
  co_advisor_id             bigint,
  latest_seminar_id         bigint,
  seminar_confirmation_code varchar(255),
  status                    varchar(255),
  phone                     varchar(255),
  major                     varchar(255),
  notes                     varchar(255),
  batch                     integer,
  email                     varchar(255) not null,
  ethnic                    varchar(255),
  religion                  varchar(255),
  dtut                      varchar(255),
  dtdt                      varchar(255),
  doan                      varchar(255),
  dang                      varchar(255),
  dot                       varchar(255),
  comments                  varchar(255),
  constraint uq_student_number unique (number),
  constraint uq_student_email unique (email),
  constraint pk_student primary key (id))
;

create table user_profile (
  id                        bigint not null,
  ituet_user_id             bigint,
  provider_id               varchar(255),
  user_id                   varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  full_name                 varchar(255),
  avatar_url                varchar(255),
  email                     varchar(255),
  auth_method               varchar(255),
  constraint pk_user_profile primary key (id))
;

create sequence faculty_seq;

create sequence image_seq;

create sequence ituet_user_seq;

create sequence seminar_seq;

create sequence student_seq;

create sequence user_profile_seq;

alter table ituet_user add constraint fk_ituet_user_student_1 foreign key (student_id) references student (id);
create index ix_ituet_user_student_1 on ituet_user (student_id);
alter table ituet_user add constraint fk_ituet_user_faculty_2 foreign key (faculty_id) references faculty (id);
create index ix_ituet_user_faculty_2 on ituet_user (faculty_id);
alter table ituet_user add constraint fk_ituet_user_main_3 foreign key (main_id) references user_profile (id);
create index ix_ituet_user_main_3 on ituet_user (main_id);
alter table student add constraint fk_student_advisor_4 foreign key (advisor_id) references faculty (id);
create index ix_student_advisor_4 on student (advisor_id);
alter table student add constraint fk_student_coAdvisor_5 foreign key (co_advisor_id) references faculty (id);
create index ix_student_coAdvisor_5 on student (co_advisor_id);
alter table student add constraint fk_student_latestSeminar_6 foreign key (latest_seminar_id) references seminar (id);
create index ix_student_latestSeminar_6 on student (latest_seminar_id);
alter table user_profile add constraint fk_user_profile_ituetUser_7 foreign key (ituet_user_id) references ituet_user (id);
create index ix_user_profile_ituetUser_7 on user_profile (ituet_user_id);



# --- !Downs

drop table if exists faculty cascade;

drop table if exists image cascade;

drop table if exists ituet_user cascade;

drop table if exists seminar cascade;

drop table if exists student cascade;

drop table if exists user_profile cascade;

drop sequence if exists faculty_seq;

drop sequence if exists image_seq;

drop sequence if exists ituet_user_seq;

drop sequence if exists seminar_seq;

drop sequence if exists student_seq;

drop sequence if exists user_profile_seq;

